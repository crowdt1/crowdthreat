# CrowdThreat
This is a replication package for "CrowdThreat: Automatically Extracting Threats, Attacks, and Mitigations from Security Posts in StackOverflow".

Our project is public at:
[https://gitlab.com/crowdt1/crowdthreat/](https://gitlab.com/crowdt1/crowdthreat/).

## Content
[1 Get Started](#1-get-started)<br>
&ensp;&ensp;[1.1 Requirements](#11-requirements)<br>
&ensp;&ensp;[1.2 Dataset](#12-dataset)<br>
&ensp;&ensp;[1.3 Train and Test](#13-train-and-test)<br>
[2 Project Summary](#2-project-summary)<br>
[3 Approach](#3-approach)<br>
[4 Experiments](#4-experiments)<br>
[5 Experimental Results](#5-experimental-results)<br>
&ensp;&ensp;[5.1 RQ1: Comparison on Threat Classification](#51-rq1-comparison-on-threat-classification)<br>
&ensp;&ensp;[5.2 RQ2: Comparison on Attack and Mitigation Summarization](#52-rq2-comparison-on-attack-and-mitigation-summarization)<br>
&ensp;&ensp;[5.3 RQ3: Component Analysis](#53-rq3-component-analysis)<br>
[6 User Study](#6-user-study)<br>
&ensp;&ensp;[6.1 User Study Procedure](#61-user-study-procedure)<br>
&ensp;&ensp;[6.2 User Study Results](#62-user-study-results)<br>

## 1 Get Started
### 1.1 Requirements
* Hardwares: NVIDIA GeForce RTX 2060 GPU, intel core i5 CPU
* OS: Ubuntu 20.04, Windows 7, 8, 10, 11
* Packages: 
  * python 3.6
  * pytorch, pytorch-transformers 1.9.0
  * cuda 11.1
  * python 2.7 (for evaluation)

### 1.2 Dataset
CrowdThreat is evaluated on Stack Overflow [data](./data). 
The structures of security posts are as follows:
* train/valid/test
  *  source.code: tokens of the source code
  *  source.comment: tokens of the source comments
  *  similar.code: tokens of the retrieved similar code
  *  similar.comment: tokens of the retrieved similar comments
  *  source.keywords: tokens of the code keywords(identifier names)


### 1.3 Train and Test
1. Go the [src](./src) directory, process the dataset:
```
cd src/preprocess
python reformat_threat_modeling.py
python data_preprocessing.py
```
2. Train CrowdThreat model:
```
python run.py
```
3. Test CrowdThreat model:
```
python test.py
```


## 2 Project Summary
Security is an increasing concern in software development and maintenance. 
Developers usually discuss threat-related knowledge, including potential threats, possible attacks, and their mitigations on developer Question and Answer (Q&A) websites. 
Being aware of such knowledge may increase the potential for developers to identify new threats while providing attack or mitigation supplements to public security resources such as CVE and NVD for known threats. 
Thus, making it invaluable for effective secure software development. 
To better achieve this awareness, we propose CrowdThreat, a deep multitask-based approach for automated extracting of threats, attacks, and mitigations from security posts found from Stack Overflow. 

1) Threat classification, which classifies security Q&A posts into the STRIDE categories by leveraging heuristic threat features, the BERT, and LSTM embedding; 
2) Attack summarization, which summarizes the attack information from security Q&A posts by leveraging heuristic features, context-aware sentence representation, and an MLP layer;
3) Mitigation summarization, which summarizes mitigation information from security Q&A posts by using a different MLP layer with the attack summarizer.

## 3 Approach
The model of diagram is available at [diagram](./diagram). The structure is shown as follow:
![Image text](diagram/model_5.png)

## 4 Experiments
3 RQs are proposed by our paper, which is related to our experiment:

- RQ1: What is the performance of CrowdThreat in threat classification?
- RQ2: What is the performance of CrowdThreat in attack and mitigation summarization?
- RQ3: How does each component in CrowdThreat contribute to threat classification, attack, and mitigation summarization?

For RQ1, we present the source paper of different baselines.
- [SmartValidator](https://arxiv.org/abs/2203.07603)
- [DEST](https://ieeexplore.ieee.org/document/9336293)
- [ThreatCNN](https://ieeexplore.ieee.org/document/8622506)

For RQ2, we present the source paper of different baselines:
- [FACTSUM](https://aclanthology.org/2020.acl-main.455/)
- [BERTSUM](https://aclanthology.org/D19-1387/)
- [TextRank](https://aclanthology.org/W04-3252/)

For RQ3,  we compare CrowdThreat with its three variants: 
1) w/o 𝑯𝒆𝒖., which removes the heuristic features from CrowdThreat,
2) w/o MTL, which trains the three models without multitask learning, and 
3) w/o BOTH, which removes the heuristic features as well
as trains the models independently.

## 5 Experimental Results
### 5.1 RQ1: Comparison on Threat Classification
<div align=center><img src="https://gitlab.com/crowdt1/crowdthreat/-/raw/main/diagram/classification.png" width="500" alt="dd-test"/></div><br>

**Answering RQ1**: CrowdThreat outperforms the five baselines in classifying threat types of security posts across the six STRIDE categories, and the average Precision, Recall, F1 and Accuracy are 76.46%, 73.09%, 74.70% and 86.85%, outperforming the best baseline SmartValidator by 12.13% (Precision), 4.77% (Recall), 8.53% (F1) and 7.25% (Accuracy).

### 5.2 RQ2: Comparison on Attack and Mitigation Summarization
<div align=center><img src="https://gitlab.com/crowdt1/crowdthreat/-/raw/main/diagram/summarization.png" width="500" alt="dd-test"/></div><br>

**Answering RQ2**: CrowdThreat outperforms the five baselines in summarizing attack and mitigation sentences. For attack summarization, CrowdThreat outperforms the best baseline by 3.75% (Precision), 7.45% (Recall), 6.15% (F1) and 7.45% (Accuracy). For mitigation summarization, CrowdThreat outperforms the best baseline by 12.15% (Precision), 8.14% (Recall), 9.99% (F1) and 6.84% (Accuracy).

### 5.3 RQ3: Component Analysis
<div align=center><img src="https://gitlab.com/crowdt1/crowdthreat/-/raw/main/diagram/component.png" width="1000" alt="dd-test"/></div><br>

**Answering RQ3**: The heuristic features 𝑯𝒆𝒖. and MLP both contribute to the performance of threat classification, attack summarization, and mitigation summarization in CrowdThreat, and multitask learning has a greater effect on improving the model than the heuristic features.

## 6 User Study
### 6.1 User Study Procedure
The survey contains three questions: 
1) Q1 (Correctness): Is the extracted threat-related knowledge correct ? (Yes or No) 
2) Q2 (Awareness): If correct, is this knowledge new to you ? (Known or Unknown) 
3) Q3 (Usefulness): For the threat-related knowledge that is new to you, how useful would you rate the extracted threat, attack, and mitigation, from 1 to 5, with 5 being the most useful?

### 6.2 User Study Results
<div align=center><img src="https://gitlab.com/crowdt1/crowdthreat/-/raw/main/diagram/user.png" width="500" alt="dd-test"/></div><br>

The majority of the extracted knowledge (87%)
from the security posts is being considered correct by the practitioners. Out of the correct threat-related knowledge, 36% is previously
unknown. This indicates that CrowdThreat can find new and
unaware threat-related knowledge. In terms of the usefulness of
such unknown knowledge, the average score for threat, attack, and
mitigation is 3.9, 4.1, and 3.8 respectively, indicating that the practitioners find them relatively useful.