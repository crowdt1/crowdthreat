# -*- coding: utf-8 -*-


import os
import pandas as pd

import openpyxl

if __name__ == '__main__':
	df = pd.read_excel('data/ThreatQueryFinal-Gen.xlsx')
	rows = df.shape[0]
	rw1 = 0
	rw2 = 300
	i = 1
	writer = pd.ExcelWriter('../data/ThreatQueryFinal-Gen-2.xlsx')
	for row in range(1, rows+1):
		if row % 300 == 0:
			sheetname = 'Sheet' + str(i)
			df2 = df.loc[rw1:rw2]
			df2.to_excel(writer, sheetname)
			rw1 += 300
			rw2 += 300
			i += 1
		if rw2 > rows:
			sheetname = 'Sheet' + str(i)
			df2 = df[rw1:rows]
			df2.to_excel(writer, sheetname)
	writer.save()
	print('处理完成!')