import pandas as pd
import re
from collections import Counter

tags_data = pd.read_csv('../data/QueryResults (3).csv')
df_threats = pd.DataFrame(tags_data)
# pattern = re.compile(r'<code>([\s\S]*?)<\/code>')

for index, tags_each in df_threats.iterrows():
    print(index)
    count_utterance = 1
    question_upd, answer_upd = '', ''
    question = tags_each['Question']
    answer = tags_each['Accepted-Answer']
    question = re.sub(r'<code>([\s\S]*?)<\/code>', '[CODE]', question)
    question = re.sub(r'<a[^>]*href="([^"]*)[^>]*>([\s\S]*?)<\/a>', '[LINK]', question)
    answer = re.sub(r'<code>([\s\S]*?)<\/code>', '[CODE]', answer)
    answer = re.sub(r'<a[^>]*href="([^"]*)[^>]*>([\s\S]*?)<\/a>', '[LINK]', answer)

    question_list = question.split('\n\n')
    answer_list = answer.split('\n\n')
    for each_question in question_list:
        # each_question = re.sub(r'<code>([\s\S]*?)<\/code>', '[CODE]', each_question)
        # each_question = re.sub(r'<a[^>]*href="([^"]*)[^>]*>([\s\S]*?)<\/a>', '[LINK]', each_question)
        each_question = each_question.replace('\n', '')
        each_question = re.sub(r'<\/?[a-zA-Z]+(\s+[a-zA-Z]+=".*")*>', '', each_question)
        each_question = '[' + str(count_utterance) + '] ' + each_question
        question_upd += (each_question + '\n')
        count_utterance += 1
    for each_answer in answer_list:
        # each_answer = re.sub(r'<code>([\s\S]*?)<\/code>', '[CODE]', each_answer)
        # each_answer = re.sub(r'<a[^>]*href="([^"]*)[^>]*>([\s\S]*?)<\/a>', '[LINK]', each_answer)
        each_answer = each_answer.replace('\n', '')
        each_answer = re.sub(r'<\/?[a-zA-Z]+(\s+[a-zA-Z]+=".*")*>', '', each_answer)
        each_answer = '[' + str(count_utterance) + '] ' + each_answer
        answer_upd += (each_answer + '\n')
        count_utterance += 1
    df_threats.loc[index, 'Question'] = question_upd
    df_threats.loc[index, 'Accepted-Answer'] = answer_upd
    # question_list.remove('')
    # answer_list.remove('')

df_threats.to_csv('data/ThreatQueryFinal-Gen.csv', encoding='utf8')
# print(df_threats)