import json
import xlrd
from importlib import import_module
STRIDE_TYPE = {'S':0,'T':1,'R':2,'I':3,'D':4,'E':5,'O':6}


def sheet_capture(sheetname):
    workbook = xlrd.open_workbook('../../data/ThreatQueryFinal-Gen-2 (1).xlsx')
    sheet_data = workbook.sheet_by_name(sheetname)
    return sheet_data


def j_reformat(sheet_data, j_set):
    model_name = "model"
    x = import_module('model.' + model_name)
    # x = import_module(model_name)
    config = x.Config()
    rows_num = sheet_data.nrows
    t_value, q_value, a_value = sheet_data.col_values(1), sheet_data.col_values(4), sheet_data.col_values(5)
    t_value, q_value, a_value = t_value[1:], q_value[1:], a_value[1:]
    t_label, a_label, m_label = sheet_data.col_values(6), sheet_data.col_values(7), sheet_data.col_values(9)
    t_label, a_label, m_label = t_label[1:], a_label[1:], m_label[1:]
    count = 0
    for (title, question, acc_answer, th_type, at_method, mitigation) in zip(t_value, q_value, a_value, t_label, a_label, m_label):
        if title == '':
            continue
        print(title)

    return j_set


if __name__ == '__main__':
    j_set = list()
    for projectid in range(10):
        sheet_data = sheet_capture('Sheet' + str(projectid+1))
        j_set = j_reformat(sheet_data, j_set)
    with open('../data/data_raw.json', 'w', encoding='utf8') as json_file:
        json.dump(j_set, json_file, ensure_ascii=False)
        print('Transfer JSON finished!')
