import pandas as pd
import re
from collections import Counter

tags_data = pd.read_csv('../data/QueryResults-tags.csv')
df_tags = pd.DataFrame(tags_data)
tags_total = ''

for index, tags_each in df_tags.iterrows():
    tag = tags_each['Tags']
    count = tags_each['num']
    tags_total += tag * count

tags_list = tags_total.split('><')
tags_list[0] = tags_list[0].replace('<', '')
tags_list[len(tags_list) - 1] = tags_list[len(tags_list) - 1].replace('>', '')
result = Counter(tags_list)
print(result)