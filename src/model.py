# coding: UTF-8
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import CrossEntropyLoss, MSELoss
from pytorch_transformers import *
import numpy as np


class Config(object):
    """配置参数"""

    def __init__(self):
        self.model_name = 'bert-base-uncased'
        self.all_sample_path = 'data/all.json'  # 所有样本路径
        self.resplit_dataset = False  # 是否重新划分数据
        self.train_path = '../data/train.json'  # 训练集
        self.dev_path = '../data/dev.json'  # 验证集
        self.test_path = '../data/test.json'  # 测试集
        self.save_path = 'saved_dict/' + self.model_name + '.ckpt'  # 模型训练结果
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')  # 设备
        self.require_improvement = 500  # 若超过1000batch效果还没提升，则提前结束训练
        # self.num_sentence_classes = 100                                 # 句子的最大长度
        self.num_epochs = 100  # epoch数
        self.batch_size = 2  # mini-batch大小
        self.word_pad_size = 20  # 每句话处理成的长度(短填长切)
        self.sentence_pad_size = 5  # 每个文档处理成的长度(短填长切)
        self.learning_rate = 5e-5  # 学习率
        self.bert_path = './bert_pretrain'
        self.tokenizer = BertTokenizer.from_pretrained(self.model_name)
        self.bert_hidden_size = 768
        self.dropout = 0.1
        self.rnn_hidden_size = 128
        self.hidden_size = 436
        self.gat_hidden_size = 128
        self.rnn_num_layers = 2
        self.knum = [512, 256, 128]
        self.ksize = [64]
        self.Cla = 128
        self.pooling_size = 10
        self.d_window = 6
        self.focal_alpha = 0.9
        self.focal_gamma = 2
        self.focal_logits = False
        self.focal_reduce = True
        self.feat_size = 52
        self.stride_categories = 7


class Model(nn.Module):

    def __init__(self, config):
        super(Model, self).__init__()
        self.config = config
        """
        bert layer
        """
        # model config (Model,Tokenizer, pretrained weights shortcut
        self.model_config = (BertModel, BertTokenizer, "bert-base-uncased")
        self.bert_tokenizer = self.model_config[1].from_pretrained(self.model_config[2])
        self.bert = self.model_config[0].from_pretrained(self.model_config[2])
        # for param in self.bert.parameters():
        #     param.requires_grad = True

        "dropout layer"
        self.dropout = nn.Dropout(config.dropout)
        self.dropout2 = nn.Dropout(config.dropout)

        "STRIDE classification layer"
        self.stride_classifier = nn.Linear(config.hidden_size, 1)

        "post embedding layer"
        self.post_layer = nn.LSTM(config.bert_hidden_size + config.rnn_hidden_size * 2, config.rnn_hidden_size, config.rnn_num_layers,
                                  batch_first=True, dropout=config.dropout)

        "contextual embedding layer"
        self.contextual_layer = nn.LSTM(config.bert_hidden_size, config.rnn_hidden_size, config.rnn_num_layers,
                            bidirectional=True, batch_first=True, dropout=config.dropout)

        "threat classification layer"
        self.threat_linear1 = nn.Linear(config.rnn_hidden_size, int(config.rnn_hidden_size / 2))
        self.threat_relu1 = nn.ReLU()
        self.threat_linear2 = nn.Linear(int(config.rnn_hidden_size / 2), int(config.rnn_hidden_size / 4))
        self.threat_relu2 = nn.ReLU()
        self.threat_linear3 = nn.Linear(int(config.rnn_hidden_size / 4), config.stride_categories)
        self.threat_relu3 = nn.ReLU()

        "attack summarization layer"
        self.attack_linear1 = nn.Linear(config.rnn_hidden_size * 2, config.rnn_hidden_size)
        self.attack_relu1 = nn.ReLU()
        self.attack_linear2 = nn.Linear(config.rnn_hidden_size, int(config.rnn_hidden_size / 2))
        self.attack_relu2 = nn.ReLU()
        self.attack_linear3 = nn.Linear(int(config.rnn_hidden_size / 2), 1)
        self.attack_relu3 = nn.ReLU()

        "mitigation summarization layer"
        self.mitigation_linear1 = nn.Linear(config.rnn_hidden_size * 2, config.rnn_hidden_size)
        self.mitigation_relu1 = nn.ReLU()
        self.mitigation_linear2 = nn.Linear(config.rnn_hidden_size, int(config.rnn_hidden_size / 2))
        self.mitigation_relu2 = nn.ReLU()
        self.mitigation_linear3 = nn.Linear(int(config.rnn_hidden_size / 2), 1)
        self.mitigation_relu3 = nn.ReLU()

        # self.sentence_classifier = nn.Linear(config.rnn_hidden_size * 2, config.sentence_pad_size)
        self.batch_size = config.batch_size
        self.sentence_pad_size = config.sentence_pad_size

    def forward(self, samples, graphs, feat_vec):
        x_list = samples[0]
        mask_list = samples[1]
        bert_hidden_tensor = None
        for index, x in enumerate(x_list):
            x_tensor = torch.tensor(x).to(self.config.device)
            mask_tensor = torch.tensor(mask_list[index]).to(self.config.device)
            # pooled or average???
            poolled_output = self.bert(x_tensor, attention_mask=mask_tensor)[1].unsqueeze(0)  # Batch size 1
            if bert_hidden_tensor is None:
                bert_hidden_tensor = poolled_output
            else:
                bert_hidden_tensor = torch.cat((bert_hidden_tensor, poolled_output), 0)

        shape = bert_hidden_tensor.size()

        "Feature Layer"
        out_feat = torch.tensor(feat_vec).cuda()

        "contextual embedding layer"
        contextual_hidden, _ = self.contextual_layer(bert_hidden_tensor)
        # contextual_hidden = contextual_hidden[:, -1, :]
        contextual_hidden = self.dropout(contextual_hidden)

        "post embedding layer"
        post_concatenation = torch.cat([bert_hidden_tensor, contextual_hidden], dim=2) # concatenate AdvS
        post_hidden, _ = self.post_layer(post_concatenation)
        post_hidden = post_hidden[:, -1, :]
        post_hidden = self.dropout2(post_hidden)

        # TASK-SPECIFIC LAYER
        "threat classification layer"
        threat_out = self.threat_linear1(post_hidden)
        threat_out = self.threat_relu1(threat_out)
        threat_out = self.threat_linear2(threat_out)
        threat_out = self.threat_relu2(threat_out)
        threat_out = self.threat_linear3(threat_out)
        threat_out = self.threat_relu3(threat_out)

        "attack summarization layer"
        attack_out = self.attack_linear1(contextual_hidden)
        attack_out = self.attack_relu1(attack_out)
        attack_out = self.attack_linear2(attack_out)
        attack_out = self.attack_relu2(attack_out)
        attack_out = self.attack_linear3(attack_out)
        attack_out = self.attack_relu3(attack_out)

        "mitigation summarization layer"
        mitigation_out = self.mitigation_linear1(contextual_hidden)
        mitigation_out = self.mitigation_relu1(mitigation_out)
        mitigation_out = self.mitigation_linear2(mitigation_out)
        mitigation_out = self.mitigation_relu2(mitigation_out)
        mitigation_out = self.mitigation_linear3(mitigation_out)
        mitigation_out = self.mitigation_relu3(mitigation_out)

        attack_out = attack_out.squeeze()
        mitigation_out = mitigation_out.squeeze()
        # attack_out = attack_out.view(-1, 3)
        # mitigation_out = mitigation_out.view(-1, 3)
        # attack_out = attack_out.reshape(self.batch_size * self.sentence_pad_size, 2)
        # mitigation_out = mitigation_out.reshape(self.batch_size * self.sentence_pad_size, 2)

        threat_out = torch.softmax(threat_out, dim=1)
        attack_out = torch.sigmoid(attack_out)
        mitigation_out = torch.sigmoid(mitigation_out)
        return attack_out, mitigation_out

