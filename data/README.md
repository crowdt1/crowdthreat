The 10-fold train/valid/test dataset is shown as follows:
- [train](../data/train.json)
- [valid](../data/dev.json)
- [test](../data/test.json)

Due to the reason of data confidentiality, 
the results of user study will be open-sourced later.